import React, { useState, useEffect, lazy, Suspense } from "react";
import ReactGA from "react-ga";
import { ImQrcode } from "react-icons/im";
import { IoLogoBitbucket } from "react-icons/io";
import Page from "./Page";
import { Avatar, Typography, Link, Box } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import constants from "../constants/constants.json";
import useStyles from "../useStyles";
import { Trans } from "@lingui/macro";
import LanguageSwitcher from "./LanguageSwitcher";

const App = () => {
    const classes = useStyles();

    const [qrData, setQrData] = useState<string>("");
    const [error, setError] = useState("");

    const QrReaderComponent = lazy(() => import("./QrReaderComponent"));
    const Output = lazy(() => import("./Output"));

    useEffect(() => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }, []);

    return (
        <>
            <LanguageSwitcher />
            <Page error={error}>
                {qrData ? (
                    <Suspense
                        fallback={
                            <p>
                                <Trans>Processing data...</Trans>
                            </p>
                        }
                    >
                        <Output qrData={qrData} setQrData={setQrData} />
                    </Suspense>
                ) : (
                    <>
                        <Box className={classes.header} mb={2}>
                            <Avatar className={classes.avatar}>
                                <ImQrcode />
                            </Avatar>
                            <Typography component="h1" variant="h5" gutterBottom>
                                <Trans>Scan your Green Certificate QR code</Trans>
                            </Typography>
                            <MuiAlert icon={false} className={classes.alert} severity="success" variant="outlined">
                                <Trans>
                                    All data are processed in your browser and are{" "}
                                    <strong>NOT sent to server nor saved anywhere</strong>.
                                    <br />
                                    You can check the source code of this web application at{" "}
                                    <Link target="_blank noopener" rel="license" href={constants.urls.source}>
                                        <IoLogoBitbucket />
                                        Bitbucket
                                    </Link>
                                </Trans>
                            </MuiAlert>
                        </Box>
                        <Suspense
                            fallback={
                                <p>
                                    <Trans>Loading QR Reader...</Trans>
                                </p>
                            }
                        >
                            <QrReaderComponent setQrData={setQrData} setError={setError} />
                        </Suspense>
                    </>
                )}
            </Page>
        </>
    );
};

export default App;
