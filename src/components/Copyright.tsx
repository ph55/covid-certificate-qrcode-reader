import React from "react";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import constants from "../constants/constants.json";
import useStyles from "../useStyles";
import { Trans } from "@lingui/macro";

export default function Copyright() {
    const classes = useStyles();
    return (
        <Typography className={classes.copyright} variant="body2" color="textSecondary" align="center">
            <Trans>
                {"Created with ❤ by "}
                <Link target="_blank" href={constants.urls.author}>
                    Peter Husárik
                </Link>
                <br />
                This work is licensed under a{" "}
                <Link
                    target="_blank noopener"
                    color="inherit"
                    rel="license"
                    href="http://creativecommons.org/licenses/by/4.0/"
                >
                    Creative Commons Attribution 4.0 International License
                </Link>
                {" as an "}
                <Link target="_blank noopener" rel="license" href={constants.urls.source}>
                    OpenSource Software
                </Link>
            </Trans>
        </Typography>
    );
}
