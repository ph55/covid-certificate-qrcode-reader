import React from "react";
// import ReactGA from "react-ga";
import { useLingui } from "@lingui/react";
import { dynamicActivate as changeLang, locales } from "../i18n";
import useStyles from "../useStyles";
import Button from "@material-ui/core/Button";
import { AppBar, ListItemIcon, ListItemText, Menu, MenuItem, Toolbar } from "@material-ui/core";
import { MdLanguage, MdRadioButtonUnchecked, MdRadioButtonChecked } from "react-icons/md";
import { Trans } from "@lingui/macro";

interface IProps {}
const LanguageSwitcher = (props: IProps) => {
    const classes = useStyles();
    const { i18n } = useLingui();
    const [anchorEl, setAnchorEl] = React.useState<(EventTarget & Element) | null>(null);

    const handleClick = (event: React.SyntheticEvent) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <AppBar color="secondary" elevation={0} className={classes.appbar} position="fixed">
            <Toolbar>
                <Button
                    aria-controls="lang-menu"
                    aria-haspopup="true"
                    variant="outlined"
                    size="small"
                    // color="primary"
                    onClick={handleClick}
                    // disableElevation
                    startIcon={<MdLanguage />}
                >
                    <Trans>Language</Trans>
                </Button>
                <Menu id="lang-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
                    {Object.entries(locales).map(([lang, langName]) => {
                        return (
                            <MenuItem
                                key={lang}
                                onClick={() => {
                                    handleClose();
                                    changeLang(lang);
                                }}
                            >
                                <ListItemIcon>
                                    {lang === i18n.locale ? <MdRadioButtonChecked /> : <MdRadioButtonUnchecked />}
                                </ListItemIcon>
                                <ListItemText>
                                    {lang === i18n.locale ? <strong> {langName}</strong> : langName}
                                </ListItemText>
                            </MenuItem>
                        );
                    })}
                </Menu>
            </Toolbar>
        </AppBar>
    );
};

export default LanguageSwitcher;
