import React from "react";
import ReactGA from "react-ga";
import { BsShieldLock } from "react-icons/bs";
import { AiOutlineReload } from "react-icons/ai";
import { List, ListItem, ListItemText, ListSubheader, Button } from "@material-ui/core";
import { IData } from "../constants/interfaces";
import MuiAlert from "@material-ui/lab/Alert";
import { DTOtype } from "../constants/enums";
import { DTO } from "../helper";
import useStyles from "../useStyles";
import { Trans } from "@lingui/macro";
import RawData from "./RawData";
import base45 from "base45";
import pako from "pako";
import cbor from "cbor";

interface IProps {
    qrData: string;
    setQrData: Function;
}

export default function Output({ qrData, setQrData }: IProps) {
    const classes = useStyles();

    const output: IData = {
        qrValue: qrData,
        version: qrData.substr(0, 3),
        base45decoded: base45.decode(qrData.substring(4)),
    };

    if (output.base45decoded) {
        try {
            output.zlibDecompressed = pako.inflate(output.base45decoded);
            output.cborObject = cbor.decodeAllSync(output.zlibDecompressed.buffer)[0];
        } catch (err) {}
        if (output.cborObject && output.cborObject.value && output.cborObject.value[2]) {
            output.cborData = cbor.decodeAllSync(output.cborObject.value[2])[0];
        }
    }

    if (output.version !== "HC1") {
        ReactGA.event({
            category: "Error",
            action: "Unsupported Certificate version",
        });
        return (
            <>
                <MuiAlert className={classes.alert} severity="error" variant="filled">
                    <Trans>Unsupported Health Certificate version ({output.version})</Trans>
                </MuiAlert>
                <br />
                <Rescan setQrData={setQrData} rawData={output.cborData} />
            </>
        );
    }

    try {
        const mainMap = output.cborData as Map<number, any>;
        const data = (mainMap.get(-260) as Map<number, any>).get(1);

        return (
            <>
                {mainMap && data && (
                    <>
                        <MuiAlert className={classes.alert} severity="warning" variant="standard">
                            <Trans>Do not share this information or your QR code with unauthorized persons!</Trans>
                        </MuiAlert>
                        <br />
                        <MuiAlert
                            icon={<BsShieldLock />}
                            className={classes.alert}
                            severity="warning"
                            variant="outlined"
                        >
                            <Trans>
                                Data electronic signature is not validated with this app, this is just simple
                                information dump!
                            </Trans>
                        </MuiAlert>
                        <List className={classes.list}>
                            <LISub>Name</LISub>
                            <LI>
                                <ListItemText>
                                    <Trans>
                                        Family name: {data.nam?.fn} ({data.nam?.fnt})
                                    </Trans>
                                </ListItemText>
                            </LI>
                            <LI>
                                <ListItemText>
                                    <Trans>
                                        Given name: {data.nam?.gn} ({data.nam?.gnt})
                                    </Trans>
                                </ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Date of birth</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{data.dob}</ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Country of Vaccination</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{data.v?.[0]?.co || data.r?.[0]?.co}</ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Certificate Issuer</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{data.v?.[0]?.is || data.r?.[0]?.is}</ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Disease or agent targeted</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{DTO(DTOtype.DISEASE, data.v?.[0]?.tg || data.r?.[0]?.tg)}</ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Date of Vaccination</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{data.v?.[0]?.dt || data.v?.[0]?.dt}</ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Dose number / Total Series of Doses</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>
                                    {data.v?.[0]?.dn || data.r?.[0]?.dn} / {data.v?.[0]?.sd || data.r?.[0]?.sd}
                                </ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Marketing Authorization Holder or manufacturer</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>
                                    {DTO(DTOtype.MANUFACTURER, data.v?.[0]?.ma || data.r?.[0]?.ma)}
                                </ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Vaccine or prophylaxis</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{DTO(DTOtype.VACCINE, data.v?.[0]?.vp || data.r?.[0]?.vp)}</ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Unique Certificate Identifier (UVCI)</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{data.v?.[0]?.ci || data.r?.[0]?.ci}</ListItemText>
                            </LI>
                            <LISub>
                                <Trans>Version</Trans>
                            </LISub>
                            <LI>
                                <ListItemText>{data.ver}</ListItemText>
                            </LI>
                        </List>
                    </>
                )}
                <Rescan setQrData={setQrData} rawData={output.cborData} />
            </>
        );
    } catch (ex) {
        ReactGA.event({
            category: "Error",
            action: "Unsupported Certificate version",
            label: `${ex.name}: ${ex.message}`,
        });
        return (
            <>
                <MuiAlert className={classes.alert} severity="error" variant="filled">
                    <Trans>Unsupported Health Certificate version ({output.version})</Trans>
                    <br />
                    <details>
                        <summary>
                            <Trans>Unexpected Error</Trans>
                        </summary>
                        {ex.name}: {ex.message}
                    </details>
                </MuiAlert>
                <br />
                <Rescan setQrData={setQrData} rawData={output.cborData} />
            </>
        );
    }
}

const LI: React.FC<{}> = ({ children }) => {
    const classes = useStyles();
    return <ListItem className={classes.listItem}>{children}</ListItem>;
};
const LISub: React.FC<{}> = ({ children }) => {
    const classes = useStyles();
    return <ListSubheader className={classes.listSubheader}>{children}</ListSubheader>;
};
const Rescan: React.FC<{ setQrData: Function; rawData: any }> = ({ setQrData, rawData }) => {
    return (
        <>
            <Button
                variant="contained"
                color="primary"
                size="large"
                startIcon={<AiOutlineReload />}
                onClick={() => {
                    setQrData("");
                }}
            >
                <Trans>Scan another QR code</Trans>
            </Button>
            <br />
            <br />
            <RawData data={rawData} />
        </>
    );
};
