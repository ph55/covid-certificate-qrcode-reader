import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import MuiAlert from "@material-ui/lab/Alert";
import Copyright from "./Copyright";
import { Box } from "@material-ui/core";
import useStyles from "../useStyles";
import { defineMessage, Trans } from "@lingui/macro";

interface IProps {
    error?: string;
}
const Page: React.FC<IProps> = ({ children, error }) => {
    const classes = useStyles();

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.bigRandomImage} />
            <Grid item xs={12} sm={8} md={5} component={Paper} square>
                <div className={classes.paper}>
                    {error && (
                        <MuiAlert className={classes.alert} severity="error" variant="filled">
                            <Trans id={getErrorMessage(error).id} />
                        </MuiAlert>
                    )}
                    {children}
                    <Box mt={5}>
                        <Copyright />
                    </Box>
                </div>
            </Grid>
        </Grid>
    );
};

const errorMessages: { [key: string]: any } = {
    "Permission denied": defineMessage({ message: "Permission denied" }),
    "Requested Device Not Found": defineMessage({ message: "Requested Device Not Found" }),
    "navigator.mediaDevices Is Undefined": defineMessage({ message: "navigator.mediaDevices Is Undefined" }),
};

const getErrorMessage = (error: string): any => {
    return errorMessages[error] || { id: error };
};

export default Page;
