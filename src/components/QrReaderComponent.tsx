import React from "react";
import ReactGA from "react-ga";
import QrReader from "react-qr-reader";
import useStyles from "../useStyles";

interface IProps {
    setQrData: (result: string) => void;
    setError: (error: string) => void;
}
const QrReaderComponent = ({ setQrData, setError }: IProps) => {
    const classes = useStyles();

    return (
        <QrReader
            onScan={(result) => {
                if (result) {
                    ReactGA.pageview("/qr");
                    ReactGA.event({
                        category: "QR",
                        action: "QR scanned",
                    });
                    setQrData(result);
                }
            }}
            onError={(error) => {
                ReactGA.pageview("/qr/error");
                ReactGA.event({
                    category: "Error",
                    action: "QR reader error",
                    label: `${error.name}: ${error.message}`,
                });
                setError(error.message);
            }}
            className={classes.reader}
        />
    );
};

export default QrReaderComponent;
