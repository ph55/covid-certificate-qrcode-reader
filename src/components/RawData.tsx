import React, { useRef, useState } from "react";
import ReactGA from "react-ga";
import { FormControlLabel, Switch } from "@material-ui/core";
import { format } from "../helper";
import useStyles from "../useStyles";
import { t } from "@lingui/macro";

interface IProps {
    data: any;
}

export default function RawData({ data }: IProps) {
    const classes = useStyles();
    const [showRaw, setShowRaw] = useState(false);
    const rawDataRef = useRef<HTMLPreElement | null>(null);

    return (
        <>
            <FormControlLabel
                control={
                    <Switch
                        color="primary"
                        checked={showRaw}
                        onChange={() => {
                            if (!showRaw) {
                                ReactGA.modalview("/raw");
                            }
                            setShowRaw((v) => !v);
                        }}
                    />
                }
                label={t`Show raw data`}
            />
            {showRaw && (
                <pre ref={rawDataRef} className={classes.pre}>
                    {format(data)}
                </pre>
            )}
        </>
    );
}
