export interface IData {
    qrValue: string;
    version?: string;
    base45decoded?: Uint8Array;
    zlibDecompressed?: Uint8Array;
    cborObject?: any;
    cborData?: any;
}
