import { DTOtype } from "./constants/enums";

export function DTO(type: DTOtype, val: string): string {
    switch (type) {
        case DTOtype.DISEASE:
            switch (val) {
                case "840539006":
                    return "COVID-19";
                default:
                    return val;
            }
        case DTOtype.VACCINE:
            switch (val) {
                case "1119305005":
                    return "SARS-CoV2 antigen vaccine";
                case "1119349007":
                    return "SARS-CoV2 mRNA vaccine";
                case "J07BX03":
                    return "covid-19 vaccines";
                default:
                    return val;
            }
        case DTOtype.MANUFACTURER:
            switch (val) {
                case "ORG-100001699":
                    return "AstraZeneca AB";
                case "ORG-100030215":
                    return "Pfizer AG";
                case "ORG-100001417":
                    return "Janssen-Cilag International";
                case "ORG-100031184":
                    return "Moderna Biotech Spain S.L.";
                case "ORG-100006270":
                    return "Curevac AG";
                case "ORG-100013793":
                    return "CanSino Biologics";
                case "ORG-100020693":
                    return "China Sinopharm International Corp. - Beijing location";
                case "ORG-100010771":
                    return "Sinopharm Weiqida Europe Pharmaceutical s.r.o. - Prague location";
                case "ORG-100024420":
                    return "Sinopharm Zhijun (Shenzhen) Pharmaceutical Co. Ltd. - Shenzhen location";
                case "ORG-100032020":
                    return "Novavax CZ AS";
                default:
                    return val;
            }
    }
}

export function format(data: any, indentLen: number = 0, indentStr: string = " "): string {
    let ret = "";
    const indentation = new Array(indentLen).fill(indentStr).join("");
    if (data instanceof Map) {
        ret += "\n";
        data.forEach((val, key) => {
            ret += `${indentation}${key}:${format(val, indentLen + 1, indentStr)}`;
        });
    } else {
        ret += `${indentation}${JSON.stringify(data, null, indentation)}\n`;
    }
    return ret;
}

export function isDevelopment() {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        // dev code
        return true;
    } else {
        // production code
        return false;
    }
}
