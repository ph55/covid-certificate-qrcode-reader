import ReactGA from "react-ga";
import { i18n } from "@lingui/core";
import { detect, fromNavigator, fromPath } from "@lingui/detect-locale";
import { t } from "@lingui/macro";
import { en, sk } from "make-plural/plurals";

export const locales = {
    en: "English",
    sk: "Slovensky",
};
export const defaultLocale = "en";

i18n.loadLocaleData({
    en: { plurals: en },
    sk: { plurals: sk },
});

/**
 * We do a dynamic import of just the catalog that we need
 * @param locale any locale string
 */
export async function dynamicActivate(locale: string) {
    const { messages } = await import(`./locales/${locale}/messages`);
    i18n.load(locale, messages);
    i18n.activate(locale);
    document.documentElement.lang = locale;
    if (fromPath(0) !== locale) {
        window.history.pushState(
            { lang: locale },
            t({ id: "Main title", message: "Green Certificate Reader" }),
            `/${locale}`
        );
        ReactGA.pageview(window.location.pathname + window.location.search);
    }
}

export function detectLocale() {
    const detectedLocale = detect(fromPath(0), fromNavigator());
    const detectedLang = detectedLocale?.split("-")[0];
    if (detectedLocale && Object.keys(locales).find((locale) => locale === detectedLocale)) {
        return detectedLocale;
    } else if (detectedLang && Object.keys(locales).find((locale) => locale === detectedLang)) {
        return detectedLang;
    } else {
        return defaultLocale;
    }
}
