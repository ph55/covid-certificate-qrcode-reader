import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import ReactGA from "react-ga";
import { I18nProvider } from "@lingui/react";
import { i18n } from "@lingui/core";
import { ThemeProvider } from "@material-ui/core/styles";
import constants from "./constants/constants.json";
import { isDevelopment } from "./helper";
import theme from "./theme";
import { dynamicActivate, detectLocale } from "./i18n";
import App from "./components/App";

ReactGA.initialize(constants.analytics.id, { testMode: isDevelopment(), debug: isDevelopment() });

const I18nApp = () => {
    useEffect(() => {
        window.onpopstate = function (e) {
            if (e.state && e.state.lang) {
                dynamicActivate(e.state.lang);
            }
        };

        dynamicActivate(detectLocale());
    }, []);

    return (
        <I18nProvider i18n={i18n}>
            <App />
        </I18nProvider>
    );
};

ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <I18nApp />
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById("root")
);

// import reportWebVitals from "./reportWebVitals";
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
