import { unstable_createMuiStrictModeTheme as createTheme } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

// const mainColor = "#3a843d";
const mainColor = green[800];
const secondaryColor = green[100];

// A custom theme for this app
const theme = createTheme({
    palette: {
        primary: {
            main: mainColor,
        },
        secondary: {
            main: secondaryColor,
        },
        error: {
            main: "#E91B0C",
        },
        background: {
            paper: "#fff",
            default: "#fff",
        },
    },
    typography: {
        h5: {
            color: mainColor,
        },
    },
});

export default theme;
