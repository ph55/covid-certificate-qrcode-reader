import { makeStyles } from "@material-ui/core";
import img01 from "./images/01.webp";
import img02 from "./images/02.webp";
import img03 from "./images/03.webp";
import img04 from "./images/04.webp";
import img05 from "./images/05.webp";
import img06 from "./images/06.webp";
import img07 from "./images/07.webp";
import img08 from "./images/08.webp";
import img09 from "./images/09.webp";
import img10 from "./images/10.webp";
import img11 from "./images/11.webp";
import img12 from "./images/12.webp";
import img13 from "./images/13.webp";

export const randomImg = (): string => {
    // backgroundImage: "url(https://source.unsplash.com/featured/?green)",
    // backgroundImage: "url(https://source.unsplash.com/featured/?covid)",

    var arr = [img01, img02, img03, img04, img05, img06, img07, img08, img09, img10, img11, img12, img13];

    return arr[Math.floor(Math.random() * arr.length)];
};

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100vh",
    },
    copyright: {
        marginBottom: 50,
    },
    bigRandomImage: {
        backgroundImage: `url(${randomImg()})`,
        backgroundRepeat: "no-repeat",
        backgroundColor: theme.palette.type === "light" ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: "cover",
        backgroundPosition: "center",
    },
    appbar: {
        alignItems: "flex-end",
        // important is needed to fix tyle specificity in bult app
        top: "auto !important",
        bottom: 0,
    },
    paper: {
        margin: theme.spacing(4, 4),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    alert: {
        width: "100%",
    },
    header: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main,
    },
    pre: {
        width: "100%",
        overflow: "auto",
        border: "solid 1px",
        borderColor: theme.palette.grey[500],
        background: theme.palette.grey[200],
        padding: theme.spacing(2),
    },
    list: {
        overflowWrap: "anywhere",
        background: theme.palette.background.paper,
    },
    listItem: {
        background: "inherit",
        padding: "0",
    },
    listSubheader: {
        background: "inherit",
        padding: theme.spacing(1, 0),
        lineHeight: "normal",
        fontWeight: "bolder",
        color: theme.palette.primary.main,
        marginTop: theme.spacing(2),
    },
    reader: {
        width: "100%",
        maxWidth: "400px",
    },
}));

export default useStyles;
